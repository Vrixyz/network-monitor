![coverage](https://gitlab.com/Vrixyz/network-monitor/badges/master/build.svg?job=test)

# network monitoring

`pip install speedtest-cli` first

Then you can test provided scripts, and when you're confident enough, create a cron, here is mine:

```
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games:/home/pi/.local/bin/

* * * * * cd ~/speedtest && ~/speedtest/fulltest.sh >> ~/speedtest/log.csv;cd -
```
