#!/bin/bash
echo `ping -c 10 google.com` | perl -lne 'print "$1,$2,$3,$4" if (/([0-9.]+)% packet loss, time [0-9.]+ms rtt min\/avg\/max\/mdev = ([0-9.]+)\/([0-9.]+)\/([0-9.]+)\/([0-9.]+) ms/)'
