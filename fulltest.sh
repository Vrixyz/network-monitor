#!/bin/bash

# Server ID,Sponsor,Server Name,Timestamp,Distance,Ping,Download,Upload,Packetloss (%),Min ping,Avg ping,Max ping

echo $HOME >> ./debug.log
echo $PATH >> ./debug.log
(>&2 cat ./debug.log)

echo "`speedtest-cli --csv`,`./ping-to-csv.sh`," | sed 's/,/;/g' | sed -e 's:\.[0123456789]*;:;:g'

